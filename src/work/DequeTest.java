package work;

import java.util.Deque;
import java.util.LinkedList;

public class DequeTest {
    public void test() {
        Deque deque = new LinkedList();
        deque.addFirst("q");
        deque.addFirst("q");
        deque.addFirst("q");
        deque.addFirst("q");
        deque.addFirst("q");
        deque.addFirst("q");
        deque.addLast("last");
        deque.offer("q");
        System.out.println(deque);
        System.out.println(deque.pollLast());
        System.out.println(deque.pollLast());
        System.out.println(deque);
    }

    public int[] twoSum(int[] nums, int target) {
        int[] io = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = 1; j < nums.length; j++) {
                if (i == j) {
                    continue;
                }
                if (nums[i] + nums[j] == target) {

                    io[0] = i;
                    io[1] = j;
                    return io;
                }
            }
        }
        return io;
    }
}

